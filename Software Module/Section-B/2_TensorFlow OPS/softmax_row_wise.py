import tensorflow as tf
import numpy as np


matrix = tf.placeholder(tf.float32)
y =  tf.reshape(matrix,[tf.shape(matrix)[0],1,tf.shape(matrix)[0]])
y = tf.nn.softmax(y,-1)

sess = tf.Session()
sess.run(tf.global_variables_initializer())
print(sess.run(y,feed_dict={matrix: [[1.,2.,3.,4.,5.],[1.,1.,0.1,5.,3.],[9.,10.,11.,12.,2.],[13.,14.,15.,16.,1.],[1.,1.,0.1,5.,3.]]}))##please input your matrix here
sess.close()
