#!/usr/bin/env python

from std_msgs.msg import String
import rospy

def string_publisher():
	string_pub = rospy.Publisher('/random_messages', String, queue_size=10)	
	rospy.init_node('stringPublisherNode', anonymous=True)

	string_to_be_published = "Hello World" 
	rospy.loginfo(string_to_be_published)
	string_pub.publish(string_to_be_published)


if __name__ == '__main__':
	string_publisher()
