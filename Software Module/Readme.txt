Submission for Team Abhiyaan 
============================ 

 
Name:  
Aakash Arul Mozhi Nambi
 
Roll Number:
ED18B001
 
 
Previous Experience:  
------------------ 
2011-2012 learnt basic electronic circuits, logic gaits
2011-2012 built a custom all terrain motor-drive robot
2012-2013 learnt arduino coding and was familiar with C&C++ coding 
2012-2013 built basic bots such as line follower, obstacle avoider 
2012-2013 built and worked in arm robots and hexapods
2013-2014 built a custom war robot with a drum mechanism powered with Ampflow E-series DC-motor, RF control and Window motors
(link for the video of https://www.youtube.com/watch?v=3C7qs2tZ6NU) #the drum robot with aluminium side plate is mine.
2014-2015 Graduated in Mechanical and Electronics from sproboticworks course graduation test
2015-2016 learnt PID control system,image processing with Roborealm,zigbee communication etc 
2016-2017 built a custom one-to-one communication between mobile robots using zigbee and locating and moving robots using color filtering(stickers are stuck on top of the robot and the camera is placed above it) from camera input
2017-2018 worked in building AGVs moving over magnetic tapes (these AGVs move over magnetic tapes with PID control and Plan their paths from user input, locating the robot is done by sensing rfid tags)  link for the product AGV :https://sproboticworks.com/shop/products/micro-agv-platform.html 
2017-2018 worked in hotword detection of an IOT module 
2018-2019 worked in developing the software-end of interactive computer vision and arm controlled robot which sorts objects,classifies faces,detects facial expression and detects finger count which is an upcoming product in www.sproboticworks.com
(thereby worked in ros platform , opencv and a bit of tensorflow for training cnn for facial expression detector)

Events Participated and Won:
--------------------------
2012-2016 won Robo Race, Robo Soccer, Robo Sumo in SPARC
2014-2015 participated in tathva14 (techfest nit calicut) 
	-qualified to semis in robowars
	-qualified to semis in deathrace(robo race)
2015-2016 participated in tathva15 
	-won 1st place in robowars
	-qualified to semis in robosoccer

 
Current PORs: 
------------- 
 
nil
 
Why I want to work in the team: 
------------------------------ 
I am fasinated about self-driving cars
 
Relevant Courses: 
---------------- 
In Institute 
------------ 

Passed 
AM1100
ED1021
ED1031
MA1101
ME1480
PH1010

Doing
ED1011
ED1033
ED2090
EE1101
MA1102 

Online 
------ 
learnt few concepts from deeplearnig.ai,sentdex tutorials,pyimage etc

