import tensorflow as tf
import numpy as np
import argparse

x = tf.placeholder(tf.float32)
x2 = x[:,::-1] ##flips horizontally
y1 = tf.matrix_band_part(x2,-1,0) ## lower left triangle
y2 = tf.matrix_band_part(x2,0,-1) ## upper right triangle
y1 = y1[:,::-1]##flips horizontally 
y2 = y2[:,::-1]##flips horizontally
z = tf.transpose(y1)##transpose
diagonal = tf.matrix_band_part(x2, 0, 0)## extracts only diagonals
diagonal = diagonal[:,::-1]
z = tf.math.add(z,y2)##flips horizontally
output = tf.math.subtract(z,diagonal)
sess = tf.Session()
sess.run(tf.global_variables_initializer())
print(sess.run(output,feed_dict={x: [[1,2,3],[4,5,6],[7,8,9]]}))
sess.close()
