#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from std_msgs.msg import Int32

rospy.init_node('batteryLevelSubscriberNode', anonymous=True)
rate = rospy.Rate(1)
battery_level_pub = rospy.Publisher('/battery_warning_light', Int32, queue_size=100)
publish_value = 0
flag1 = 0
flag2 = 0
def battery_level_callback(battery_level_data):
	global flag1
	global flag2
	global publish_value
	if battery_level_data.data < 33:
		if flag1 == 0:
			rospy.loginfo('Received Battery Level < 33%'+str(battery_level_data.data))
			flag1 = 1
		publish_value = 0 if publish_value else 1
	
	else:
		publish_value = 1
		if flag2 == 0:
			rospy.loginfo('Received Battery Level > 33%'+str(battery_level_data.data))
			flag2 = 1
	
		
	battery_level_pub.publish(publish_value)
	rospy.loginfo('Publishing'+str(publish_value))
	rate.sleep()
		
	
		
    
def battery_level_listener():

    
    rospy.Subscriber("/battery_level_messages", Int32, battery_level_callback)
    rospy.spin()

if __name__ == '__main__':
    battery_level_listener()
