#!/usr/bin/env python

from std_msgs.msg import Int32
import rospy

def string_publisher():
	battery_level_pub = rospy.Publisher('/battery_level_messages', Int32, queue_size=100)	
	rospy.init_node('batteryLevelPublisherNode', anonymous=True)
	rate = rospy.Rate(1)
	
	for battery_level in range(0,100): 
		rospy.loginfo(100-battery_level)
		battery_level_pub.publish(100-battery_level)
		rate.sleep()


if __name__ == '__main__':
	string_publisher()
