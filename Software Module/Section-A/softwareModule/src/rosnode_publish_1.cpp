#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>


int main(int argc, char **argv)
{

  ros::init(argc, argv, "stringPublisher");
  ros::NodeHandle stringPublisher;
  ros::Publisher string_pub = stringPublisher.advertise<std_msgs::String>("/random_messages", 1000);//The second argument is the size of publishing queue which is not required here as we are not publishing continuously
 
  std_msgs::String msg;
  std::stringstream ss;
  ss << "Hello World ";
  msg.data = ss.str();

  ROS_INFO("%s", msg.data.c_str());
  string_pub.publish(msg);


  return 0;
}
